#!/bin/bash -xe
### This script is useful for moving to another branch.
### Expects the git repository of libreoffice in a
### directory with the same name as the branch.
###
### Example usage:
###    cp -a libreoffice-6-2-2 libreoffice-6-2-3
###    ./git-checkout.sh libreoffice-6-2-3

source /host/settings.sh
branch=${1:-$GIT_BRANCH}
cd /host/$branch

time nice git reset
time nice git checkout .
time nice git clean -fdx
time nice ./g checkout $branch

git status
