#!/bin/bash -x
### Compile the code of libreoffice, according to the instructions at:
### https://wiki.documentfoundation.org/Development/BuildingOnLinux
### https://wiki.documentfoundation.org/Development/Linux_Build_Dependencies
###
### Usage: ./compile.sh [<git-branch> <lang>]
###
### If <git-branch> and <lng> are missing, then GIT_BRANCH and LANG from
### /host/settings.sh are used.

### go to the host directory
cd /host/

### get the GIT_BRANCH and LANG
source /host/settings.sh

### make sure that the script is called with `nice ... | tee $logfile`
if [[ "$1" != "calling_myself" ]]; then
    # this script has *not* been called recursively by itself
    GIT_BRANCH=${1:-$GIT_BRANCH}
    datestamp=$(date +%F | tr -d -)
    logfile=logs/$GIT_BRANCH-$datestamp.log
    rm -f $logfile
    mkdir -p logs/
    nice "$0" "calling_myself" "$@" | tee $logfile
    exit
else
    # this script has been called recursively by itself
    shift # remove the termination condition flag in $1
fi

### make sure that ccache is enabled
export CCACHE_DIR=/host/ccache
export CCACHE_BASEDIR=`readlink -f .`
export CCACHE_COMPRESS=1
ccache --max-size 32G

### get the start time
start_time=$(date)

### stop on error
set -e

### go to the source directory
### and make sure that we have the latest version
GIT_BRANCH=${1:-$GIT_BRANCH}
LANG=${2:-$LANG}
cd $GIT_BRANCH/
time ./g pull -r

### start the compilation
time ./autogen.sh --with-lang="$LANG"
time make # build-nocheck
#time make check

### get the end time
end_time=$(date)

### print the start and end times
set +x
echo ================================================================
echo "Start time : $start_time"
echo "End time   : $end_time"
echo ================================================================
