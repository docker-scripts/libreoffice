#!/bin/bash -x
### get the source code

source /host/settings.sh

branch=${1:-$GIT_BRANCH}
lng=${2:-$LANG}

### get the version of translations from the branch name
v=$(echo $branch | cut -d- -f2,3 | tr -d -)
libo_ui="libo${v}_ui"
libo_help="libo${v}_help"

### update UI translations
wget -O $libo_ui.zip \
     "https://translations.documentfoundation.org/export/?path=/$lng/$libo_ui/"
unzip $libo_ui.zip
cp -R $lng-$libo_ui/$lng/$libo_ui/* \
   $branch/translations/source/$lng/

mo_file_mapping="
    acc.mo:accessibility
    avmedia.mo:avmedia
    basctl.mo:basctl
    chart.mo:chart2
    cnr.mo:connectivity
    cui.mo:cui
    dba.mo:dbaccess
    dkt.mo:desktop
    editeng.mo:editeng
    filter:filter
    for.mo:formula
    fps.mo:fpciker
    frm.mo:forms
    fwk.mo:framework
    pcr.mo:extensions
    rpt.mo:reportdesign
    sb.mo:basic
    sc.mo:sc
    sca.mo:scaddins
    scc.mo:sccomp
    sd.mo:sd
    sfx.mo:sfx2
    sm.mo:starmath
    svl.mo:svl
    svt.mo:svtools
    svx.mo:svx
    sw.mo:sw
    uui.mo:uui
    vcl.mo:vcl
    wiz.mo:wizards
    wpt.mo:writerperfect
    xsc.mo:xmlsecurity
"
for m in $mo_file_mapping; do
    mo_file=$(echo $m | cut -d: -f1)
    po_dir=$(echo $m | cut -d: -f2)
    msgfmt -o $branch/instdir/program/resource/$lng/LC_MESSAGES/$mo_file \
           $branch/translations/source/$lng/$po_dir/messages.po
done

### update Help translations
wget -O $libo_help.zip \
     "https://translations.documentfoundation.org/export/?path=/$lng/$libo_help/"
unzip $libo_help.zip
mkdir -p $branch/translations/source/$lng/helpcontent2/source/text/
cp -fR $lng-$libo_help/$lng/$libo_help/* \
   $branch/translations/source/$lng/helpcontent2/source/text/
mv $branch/translations/source/$lng/helpcontent2/source/text/auxiliary.po \
   $branch/translations/source/$lng/helpcontent2/source/

### build l10n
cd $branch
make build-l10n-only
cd ..

#### clean up
rm $libo_ui.zip $libo_help.zip
rm -rf $lng-$libo_ui $lng-$libo_help
