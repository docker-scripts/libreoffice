#!/bin/bash -x
### get the source code

source /host/settings.sh
branch=${1:-$GIT_BRANCH}
git clone --branch=$branch git://anongit.freedesktop.org/libreoffice/core $branch
