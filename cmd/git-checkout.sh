cmd_git-checkout_help() {
    cat <<_EOF
    git-checkout [libreoffice-x-y-z]
        Checkout a git branch in the corresponding directory.
        If no branch ig given, than GIT_BRANCH from 'settings.sh' is used.
        A directory with the same name as the branch has to exist first,
        otherwise it will fail.

_EOF
}

cmd_git-checkout() {
    local branch=${1:-$GIT_BRANCH}
    if [[ -d $branch ]]; then
        ds inject git-checkout.sh $branch
    else
      echo "
First copy the code of an old branch to a new directory like this:
    cp -a $GIT_BRANCH $branch
Then run:
    ds git-checkout $branch
"
      exit 1
    fi
}
