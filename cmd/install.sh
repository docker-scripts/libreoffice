cmd_install_help() {
    cat <<_EOF
    compile [<libreoffice-x-y-z> <lang>]
        Install a compiled branch and language inside the container.
        If the branch and the language are not given,
        the values of GIT_BRANCH and LANG from 'settings.sh' are used.

_EOF
}

cmd_install() {
    local branch=${1:-$GIT_BRANCH}
    local lng=${2:-$LANG}

    ds inject update-translations.sh $branch $lng

    cd ..
    ds inject lo-install.sh $branch $lng
}
