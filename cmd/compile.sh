cmd_compile_help() {
    cat <<_EOF
    compile [<libreoffice-x-y-z> <lang>]
        Run the compile scripts inside the container.
        If the branch and the language are not given,
        the values of GIT_BRANCH and LANG from 'settings.sh' are used.

_EOF
}

cmd_compile() {
    local branch=${1:-$GIT_BRANCH}
    local lng=${2:-$LANG}
    local datestamp=$(date +%F | tr -d -)

    ds inject git-checkout.sh $branch
    ds inject compile.sh $branch $lng
    ds inject update-translations.sh $branch $lng

    echo "Check also the logs:  less -r logs/$branch-$datestamp.out"
}
