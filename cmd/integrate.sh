cmd_integrate_help() {
    cat <<_EOF
    integrate
        Integrate libreoffice with the desktop container.

_EOF
}

cmd_integrate() {
    cd ..

    mkdir -p scripts/
    cp $APPDIR/misc/lo-setup.sh scripts/
    ds inject lo-setup.sh $LANG
    rm scripts/lo-setup.sh
        
    cp $APPDIR/misc/lo-install.sh scripts/
    ds inject lo-install.sh $GIT_BRANCH $LANG
    
    $APPDIR/misc/setup-cronjob.sh
}
