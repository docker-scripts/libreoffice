#!/bin/bash -x

file=/usr/local/bin/install-libreoffice.sh
path=$(realpath .)
cat <<EOF > $file
#!/bin/bash
cd $path
ds inject lo-install.sh
EOF
chmod +x $file
cat <<EOF > /etc/cron.d/install-libreoffice
0 2 * * *  root  $file > /dev/null 2>&1
EOF

file=/usr/local/bin/compile-libreoffice.sh
cat <<EOF > $file
#!/bin/bash
cd $path
cd libreoffice
ds compile
cd ..
ds inject lo-install.sh
EOF
chmod +x $file
cat <<EOF > /etc/cron.d/compile-libreoffice
0 2 * * 0  root  $file > /dev/null 2>&1
EOF
