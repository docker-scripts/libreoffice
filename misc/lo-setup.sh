#!/bin/bash -x

lng=${1:-en}

### setup language
sed -i /etc/locale.gen -e "/UTF-8/ s/^# $lng/$lng/"
locale-gen
line=$(grep ^$lng /etc/locale.gen | head -1)
lang=$(echo $line | cut -d' ' -f1)
language=$(echo $lang | cut -d. -f1)
language+=":$lng"
update-locale --reset LANG="$lang" LANGUAGE="$language"

### setup menu item
cat <<EOF > /usr/share/applications/libreoffice.desktop
[Desktop Entry]
Version=1.0
Terminal=false
NoDisplay=false
Icon=libreoffice-startcenter
Type=Application
Categories=Office;X-Red-Hat-Base;X-SuSE-Core-Office;X-MandrivaLinux-Office-Other;
Exec=/usr/local/lib/libreoffice/program/soffice %U
MimeType=application/vnd.openofficeorg.extension;x-scheme-handler/vnd.libreoffice.cmis;
Name=LibreOffice
GenericName=Office
Comment=The office productivity suite compatible to the open and standardized ODF document format. Supported by The Document Foundation.
StartupNotify=true
X-GIO-NoFuse=true
StartupWMClass=libreoffice-startcenter
X-KDE-Protocols=file,http,ftp,webdav
X-AppStream-Ignore=True
NotShowIn=GNOME;

##Define Actions
Actions=Writer;Calc;Impress;Draw;Base;Math;

[Desktop Action Writer]
Name=Writer
Exec=/usr/local/lib/libreoffice/program/soffice --writer
EOF
