#!/bin/bash -x

cd /host/
source libreoffice/settings.sh
branch=${1:-$GIT_BRANCH}
rm -f $branch
ln -s libreoffice/$branch .
cd $branch
make install
