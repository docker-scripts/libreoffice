# Compiling LibreOffice with the latest translations.

## Standalone

### Installation

- First install `ds`: https://gitlab.com/docker-scripts/ds#installation

- Then get the scripts: `ds pull libreoffice`

- Create a directory for the container: `ds init libreoffice @libreoffice`

- Fix the settings: `cd /var/ds/libreoffice/ ; vim settings.sh`

- Build image, create the container, configure it, and clone the
  source code of LibreOffice: `ds make`

### Compilation

To start the compilation run:
```
ds git-checkout
ds compile
```

The compilation process may take several hours. When the compilation
is finished successfully, the new program will be in:
`libreoffice-6-2-2/instdir/program/soffice`.

To compile a branch that is different from **GIT_BRANCH** on
`settings.sh`, we first have to create a directory with the code of
this branch, and after that compile it:
```
cp -a libreoffice-6-2-2 libreoffice-6-2-3
ds git-checkout libreoffice-6-2-3
ds compile libreoffice-6-2-3
```

To only update translations run: `ds inject update-translations.sh`

### Other commands

```
ds stop
ds start
ds shell
ds help
```

## Integrated

This container will be integrated with a desktop container, so that
the compiled program can be installed there and can be checked from a
browser, without needing to copy and install it locally. This will
also create cron jobs for re-compiling libreoffice weekly, and for
updating translations from Pootle nightly.

### Installation

- First install `ds`: https://gitlab.com/docker-scripts/ds#installation

- Then install `desktop`: https://gitlab.com/docker-scripts/desktop#installation
  Let's assume that it is installed on `/var/ds/desk.example.org`

- Get the scripts: `ds pull libreoffice`

- Create a directory for the libreoffice container:
  ```
  cd /var/ds/desk.example.org/
  mkdir libreoffice
  cd libreoffice
  ds init libreoffice
  ```

- Fix the settings: `vim settings.sh`

- Build the container: `ds make`

- Integrate it with the desktop container: `ds integrate`


### Compiling and Installing LibreOffice

To compile:
```
cd /var/ds/desk.example.org/
cd libreoffice
ds compile
```

To only update translations:
```
cd /var/ds/desk.example.org/
cd libreoffice
ds inject update-translations.sh
```

To install (this will also update translations):
```
cd /var/ds/desk.example.org/
ds inject lo-install.sh
```

### Customize Cron Jobs

LibreOffice re-compilation is done every sunday night, and updating
translations with the latest version from Pootle is done nightly.  If
you want to change these settings, edit
`/etc/cron.d/compile-libreoffice` and
`/etc/cron.d/install-libreoffice`.