include(focal)

RUN apt install --yes git vim wget bash-completion bzip2
RUN echo 'deb-src http://archive.ubuntu.com/ubuntu focal main' > /etc/apt/sources.list.d/debsrc.list\
    && apt update \
    && apt build-dep --yes libreoffice
RUN apt install --yes libgconf2-dev gstreamer1.0-libav libkrb5-dev nasm graphviz ccache libpython3-dev
